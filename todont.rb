# frozen_string_literal: true

require 'gitlab'

client = Gitlab::Client.new(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV.fetch('GITLAB_TOKEN')
)

to_remove = []

client.todos.auto_paginate do |todo|
  case todo.action_name
  when 'approval_required', 'build_failed'
    to_remove.push(todo.id)
  end
end

to_remove.each do |id|
  client.mark_todo_as_done(id)
end

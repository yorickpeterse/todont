# Todont

Todont is a tiny Ruby script for automatically marking certain GitLab TODOs as
done, reducing the amount of noise these TODOs would cause.

Two examples of such TODOs are TODOs created when you are added as an approver,
and TODOs created because of build failures.

Approver TODOs are useless, because just being added as an approver doesn't mean
you have to take a look _right now_. In fact, more often than not such TODOs
arrive too early as they are often created when a merge request is created, not
when it's ready for review.

Build failure TODOs are not helpful either. If you're still working on a merge
request and it has build failures, you'll notice anyway; the TODO doesn't really
help with that.

## Requirements

* Ruby
* A GitLab private token for your account

## Usage

Install the dependencies:

    bundle install

Run the script:

    env GITLAB_TOKEN='hunter2' ruby todont.rb

You can also run the script using a cronjob, or a GitLab CI schedule.

## License

All source code in this repository is licensed under the Mozilla Public License
version 2.0, unless stated otherwise. A copy of this license can be found in the
file "LICENSE".
